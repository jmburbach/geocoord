# Copyright (C) 2009 Jacob Burbach <jmburbach@gmail.com>
#
# Formulae also adapted from various sources on the net including:
#
#- Latitude/longitude spherical geodesy formulae & scripts (c) Chris Veness 2002-2009
#- http://www.movable-type.co.uk/scripts/latlong.html
# 
#- http://williams.best.vwh.net/avform.htm
#
################################################################################
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
################################################################################
import math

try:
	math.isnan
except:
	# python < 2.6 
	math.isnan = lambda f: f != f


MEAN_EARTH_RADIUS_KM = 6371.0
NM_TO_RAD = (math.pi / (180.0 * 60.0))
RAD_TO_NM = ((180.0 * 60.0) / math.pi)
KM_TO_NM = 0.539956803


class GeoCoord(object):

	def __init__(self, latitude, longitude):
		self.__longitude = float(longitude)
		self.__latitude = float(latitude)
		self.__longitude_rad = math.radians(self.__longitude)
		self.__latitude_rad = math.radians(self.__latitude)

	@property
	def longitude(self):
		return self.__longitude
	
	@property
	def latitude(self):
		return self.__latitude

	@property
	def longitude_rad(self):
		return self.__longitude_rad

	@property
	def latitude_rad(self):
		return self.__latitude_rad

	def intermediate(self, coord, factor):
		""" Return a GeoCoord of a point some `factor' along a great
		circle route between ourself and `coord'"""
		D = self.distance(coord) * NM_TO_RAD
		F = factor
		lat1 = self.latitude_rad
		lon1 = self.longitude_rad
		lat2 = coord.latitude_rad
		lon2 = coord.longitude_rad

		A = math.sin((1.0 - F) * D) / math.sin(D)
		B = math.sin(F * D) / math.sin(D)
		x = A * math.cos(lat1) * math.cos(lon1) + B * math.cos(lat2) * math.cos(lon2)
		y = A * math.cos(lat1) * math.sin(lon1) + B * math.cos(lat2) * math.sin(lon2)
		z = A * math.sin(lat1) + B * math.sin(lat2)
		lat = math.atan2(z, math.sqrt(x ** 2 + y ** 2))
		lon = math.atan2(y, x)

		return GeoCoord(math.degrees(lat), math.degrees(lon))

	def destination(self, true_course_deg, distance_nm):
		"""Given a true course in degrees and a distance in nautical
		miles, return a GeoCoord representing that position."""
		R = math.radians(true_course_deg)
		D = distance_nm * NM_TO_RAD
		lat1 = self.latitude_rad
		lon1 = self.longitude_rad

		lat2 = math.asin(math.sin(lat1) * math.cos(D) + math.cos(lat1) * math.sin(D) * math.cos(R))
		if math.cos(lat2) == 0:
			lon2 = lon1
		else:
			lon2 = ((lon1 - math.asin(math.sin(R) * math.sin(D) / math.cos(lat2)) + math.pi) % (2 * math.pi)) - math.pi

		return GeoCoord(math.degrees(lat2), math.degrees(lon2))

	def distance(self, coord):
		""" Return the distance, in nautical miles, from
		ourself to another GeoCoord """
		lon1 = self.longitude_rad 
		lat1 = self.latitude_rad
		lon2 = coord.longitude_rad
		lat2 = coord.latitude_rad
		dlon = lon2 - lon1
		dlat = lat2 - lat1
		a = ( math.sin(dlat * 0.5) * math.sin(dlat * 0.5) +
				math.cos(lat1) * math.cos(lat2) *
				math.sin(dlon * 0.5) * math.sin(dlon * 0.5) )
		c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
		return MEAN_EARTH_RADIUS_KM * c * KM_TO_NM

	def bearing(self, coord):
		""" Return the initial (true) bearing from ourself to another GeoCoord """
		lat1 = self.latitude_rad
		lat2 = coord.latitude_rad
		dlon = coord.longitude_rad - self.longitude_rad
		y = math.sin(dlon) * math.cos(lat2)
		x = ( math.cos(lat1) * math.sin(lat2) -
				math.sin(lat1) * math.cos(lat2) * math.cos(dlon) )
		a = math.atan2(y, x)
		return (math.degrees(a) + 360) % 360

	def midpoint(self, coord):
		""" Return GeoCoord  midpoint of great circle line
		between ourself and another GeoCoord """
		lat1 = self.latitude_rad
		lat2 = coord.latitude_rad
		dlon = coord.longitude_rad - self.longitude_rad
		bx = math.cos(lat2) * math.cos(dlon)
		by = math.cos(lat2) * math.sin(dlon)
		lat3 = math.atan2(math.sin(lat1) + math.sin(lat2), 
					math.sqrt((math.cos(lat1) + bx) * (math.cos(lat1) + bx) + by * by) )
		lon3 = self.longitude_rad + math.atan2(by, math.cos(lat1) + bx)
		if math.isnan(lat3) or math.isnan(lon3):
			return None
		return GeoCoord(math.degrees(lon3), math.degrees(lat3))

	def __str__(self):
		return "GeoCoord <%.5f latitude, %.5f longitude>" % (self.latitude, self.longitude)

	def gcmap_string(self):
		lat = str(self.latitude)
		lon = str(self.longitude)
		if lat[0] == "-":
			lat = lat[1:] + "S"
		else:
			lat += "N"
		if lon[0] == "-":
			lon = lon[1:] + "W"
		else:
			lon += "E"

		return "%s %s" % (lat, lon)


if __name__ == "__main__":
	g1 = GeoCoord(37.721278, -122.220721) # KOAK
	g2 = GeoCoord(21.318681, -157.922428) # PHNL
	
	coords = []
	coords.append( g1.gcmap_string() )
	for i in range (1, 10):
		factor = 0.1 * i
		gi = g1.intermediate(g2, factor)
		coords.append(gi.gcmap_string())		
	coords.append(g2.gcmap_string())

	for i in range(0, len(coords)):
		c1 = coords[i]
		try:
			c2 = coords[i+1]
			print c1 + "-" + c2
		except:
			print c1
